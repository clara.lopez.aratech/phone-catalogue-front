import {PHONE_LOADED, LOADING_PHONE, LOADING_PHONE_ERROR}  from '../actions/currentPhoneActions';

const initialState = {
    phone: {},
    phonePending: false,
    phoneError: '',
}
const currentPhone = (state = initialState, action) => {
    switch(action.type) {
        case LOADING_PHONE:
            return Object.assign({}, state, {
                phonePending: true,
            });
        case PHONE_LOADED:
            return Object.assign({}, state, {
                phonePending: false,
                phone: action.payload
            });
        case LOADING_PHONE_ERROR:
            return Object.assign({}, state, {
                phonePending: false,
                phoneError: action.payload,
            });
        default:
            return state;
    }
}

export default currentPhone;