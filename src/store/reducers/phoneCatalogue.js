import {LOADING_DATA, DATA_LOADED, LOADING_ERROR}  from '../actions/catalogueActions';

const initialState = {
    catalogue: [],
    pending: true,
    error: '',
}
const phoneCatalogue = (state = initialState, action) => {
    switch(action.type) {
        case LOADING_DATA:
            return Object.assign({}, state, {
                pending: true,
            });
        case DATA_LOADED:
            return Object.assign({}, state, {
                pending: false,
                catalogue: action.payload
            });
        case LOADING_ERROR:
            return Object.assign({}, state, {
                pending: false,
                error: action.payload,
            });
        default:
            return state;
    }
}

export default phoneCatalogue;