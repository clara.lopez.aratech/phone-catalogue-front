import Axios from 'axios';

export const DATA_LOADED = "DATA_LOADED";
export const LOADING_DATA = "LOADING_DATA";
export const LOADING_ERROR = "LOADING_ERROR";

export function getData() {
  return function(dispatch) {
    dispatch({ type: "LOADING_DATA"});
    setTimeout(() => {
    Axios.get('http://localhost:3001/phones')
      .then(response => {
        dispatch({ type: "DATA_LOADED", payload: response.data });
      })
      .then(response => response,
        error => dispatch({type: "LOADING_ERROR", payload: error})
      )
    }, 1000);
  };
}