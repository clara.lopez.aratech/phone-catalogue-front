import Axios from 'axios';

export const PHONE_LOADED = "PHONE_LOADED";
export const LOADING_PHONE = "LOADING_PHONE";
export const LOADING_PHONE_ERROR = "LOADING_PHONE_ERROR";

export function getPhone(id) {
  return function(dispatch) {
    dispatch({ type: "LOADING_PHONE" });
    setTimeout(() => {
    Axios.get('http://localhost:3001/phones/' + id)
      .then(response => {
        dispatch({ type: "PHONE_LOADED", payload: response.data });
      })
      .then(response => response,
        error => dispatch({type: "LOADING_PHONE_ERROR", payload: error})
      )
    }, 1000);
  };
}