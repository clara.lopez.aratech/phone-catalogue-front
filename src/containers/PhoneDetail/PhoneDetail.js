import React, {Component} from 'react';
import { connect } from 'react-redux';
import { getPhone } from "../../store/actions/currentPhoneActions";

import Detail from '../../components/Detail';
import Spinner from '../../components/Spinner';

import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class PhoneDetail extends Component {

    componentDidMount() {
        this.props.getPhone(this.props.match.params.id);
    }

    render () {
        const {phonePending, phone, error} = this.props;
        if (phonePending) {
            return <Spinner/>
        } else if (error) {
            return <Alert severity="error">Opps! Something went wrong.</Alert>
        } else {
            return (
                <div>
                    <Detail phone = {phone}/>
                </div>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        phonePending: state.currentPhone.phonePending,
        phone: state.currentPhone.phone,
        error: state.currentPhone.phoneError,
    };
};

const mapDispathToProps = {
    getPhone
};

export default connect(mapStateToProps, mapDispathToProps)(PhoneDetail);