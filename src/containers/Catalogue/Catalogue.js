import React, {Component} from 'react';
import { connect } from 'react-redux';
import { getData } from "../../store/actions/catalogueActions";

import Spinner from '../../components/Spinner';
import Grid from '@material-ui/core/Grid';
import Card from '../../components/Card';

import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Catalogue extends Component {

    componentDidMount() {
        this.props.getData();
    }

    render () {
        const {catalogue, pending, error} = this.props;
        if (pending) {
            return <Spinner/>
        } else if (error){
            return <Alert severity="error">Opps! Something went wrong.</Alert>
        } else {
            return (
                <div>
                    <Grid container spacing={1}>
                        {catalogue.map(el => (
                        <Card key={el.id} phone = {el}/>
                        ))}
                    </Grid>
                </div>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        catalogue: state.phoneCatalogue.catalogue,
        pending: state.phoneCatalogue.pending,
        error: state.phoneCatalogue.error,
    };
};

const mapDispathToProps = {
    getData
};


export default connect(mapStateToProps, mapDispathToProps)(Catalogue);