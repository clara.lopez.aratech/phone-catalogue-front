import React from 'react';
import './App.css';
import Catalogue from './containers/Catalogue/Catalogue';
import Navbar from './components/Navbar'
import PhoneDetail from './containers/PhoneDetail/PhoneDetail';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Route path="/" exact component={Catalogue}/>
        <Route path="/phone-detail/:id" exact component={PhoneDetail}/>
      </div>
    </BrowserRouter>
  );
}

export default App;
