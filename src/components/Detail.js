import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: '5%',
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  section1: {
    margin: theme.spacing(3, 2),
  },
  section2: {
    margin: theme.spacing(2),

  },
  section3: {
    margin: theme.spacing(3, 1, 1),
  },
  info: {
    'text-align': 'left',
  },
  container: {
    'align-items': 'center',
  },
  img: {
    height: '50vh',
    'background-size': 'contain',
  }
}));

function MiddleDividers(props) {
  const { phone } = props;
  const classes = useStyles();

  if (phone) {
    return (
      <div>
        <Grid container className={classes.container}>
          <Grid item xs={12} md={6}>
          <CardMedia
            className={classes.img}
            image={"http://localhost:3001/images/" + phone.imageFileName}
            title="Contemplative Reptile"
            />
            {/* <img className={classes.img} src={"http://localhost:3001/images/" + phone.imageFileName}></img> */}
          </Grid>
          <Grid item xs={12} md={6} className={classes.root}>
            <div className={classes.section1}>
              <Grid container alignItems="center">
                <Grid item xs>
                  <Typography gutterBottom variant="h4">
                    {phone.name}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography gutterBottom variant="h6">
                    {phone.price}€
                  </Typography>
                </Grid>
              </Grid>
              <Typography gutterBottom variant="body1">
                {phone.manufacturer}
              </Typography>
              <Typography color="textSecondary" variant="body2">
                {phone.description}
              </Typography>
            </div>
            <Divider variant="middle" />
            <div className={classes.section2}>
              <Typography className={classes.info} gutterBottom variant="body1">
                Features
              </Typography>
              <Typography className={classes.info} color="textSecondary" gutterBottom variant="body2">
                Screen: {phone.screen}
              </Typography>
              <Typography className={classes.info} color="textSecondary" gutterBottom variant="body2">
                Processor: {phone.processor}
              </Typography>
              <Typography className={classes.info} color="textSecondary" gutterBottom variant="body2">
                Ram: {phone.ram}
              </Typography>
              <Typography className={classes.info} color="textSecondary" gutterBottom variant="body2">
                Color: {phone.color}
              </Typography>
              <div>
                <Chip className={classes.chip} label={phone.manufacturer} />
                <Chip className={classes.chip} label={phone.processor} />
                <Chip className={classes.chip} label={phone.screen} />
                <Chip className={classes.chip} label={phone.color} />
              </div>
            </div>
            <div className={classes.section3}>
              <Button color="primary">Add to cart</Button>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  } 
}

export default MiddleDividers;
