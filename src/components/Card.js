import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  media: {
    height: 240,
    'background-size': 'contain',
  },
  link: {
    'text-decoration': 'none',
  }
});

function CardComponent(props) {
  const { phone } = props;
  const classes = useStyles();

  return (
    <Grid item xs={12} md={3}>
        <Card>
        <CardActionArea>
            <Link  
                className={classes.link}
                to={{
                    pathname: '/phone-detail/' + phone.id
                }}>
            <CardMedia
            className={classes.media}
            image={"http://localhost:3001/images/" + phone.imageFileName}
            title=""
            />
            </Link>
            <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
                {phone.name}
            </Typography>
            <Typography noWrap variant="body2" color="textSecondary" component="p">
                {phone.description}
            </Typography>
            <Typography align="right" noWrap variant="h5" component="h2">
                {phone.price} €
            </Typography>
            </CardContent>
        </CardActionArea>
        <CardActions>
            <Button size="small" color="primary">
            Add to card
            </Button>
            <Link  
                className={classes.link}
                to={{
                    pathname: '/phone-detail/' + phone.id
                }}>
                <Button size="small" color="primary">
                    Learn More
                </Button>
            </Link>
        </CardActions>
        </Card>
    </Grid>
  );
}

export default CardComponent;